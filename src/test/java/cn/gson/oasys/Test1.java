package cn.gson.oasys;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;


import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.stuxuhai.jpinyin.PinyinException;
import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;

import cn.gson.oasys.mappers.NoticeMapper;
import cn.gson.oasys.model.dao.attendcedao.AttendceService;
import cn.gson.oasys.model.dao.processdao.NotepaperDao;
import cn.gson.oasys.model.dao.user.UserDao;

@SpringBootTest
@RunWith(SpringRunner.class)
public class Test1 {
	Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private NotepaperDao notepaperDao;
	
	@Autowired
	private NoticeMapper nm;
	
	@Autowired
	AttendceService attendceService;
	@Autowired
	UserDao uDao;

	@Test
	public void test(){
		PageHelper .startPage(0, 10);
		List<Map<String, Object>> list=nm.findMyNotice(1L);
		PageInfo<Map<String, Object>> info=new PageInfo<Map<String, Object>>(list);
		logger.info(JSON.toJSONString(info));
	}
	
	@Test
	public void test2(){
	/*	String str="罗祥";
		try {
			logger.info(PinyinHelper.convertToPinyinString(str, "",PinyinFormat.WITH_TONE_MARK));
			logger.info(PinyinHelper.convertToPinyinString(str, "",PinyinFormat.WITH_TONE_NUMBER));
			logger.info(PinyinHelper.convertToPinyinString(str, "",PinyinFormat.WITHOUT_TONE));
			logger.info(PinyinHelper.getShortPinyin(str));
		} catch (PinyinException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		DecimalFormat numberFormat= (DecimalFormat)DecimalFormat.getInstance();
		numberFormat.applyPattern("000");

		for (int i=1 ;i<=130;i++){

			logger.info("INSERT INTO `oasys`.`aoa_user`( `address`, `bank`, `birth`, `eamil`, `father_id`, `hire_time`, `user_idcard`, `img_path`, `is_lock`, `last_login_ip`, `last_login_time`, `modify_time`, `modify_user_id`, `password`, `real_name`, `salary`, `user_school`, `sex`, `theme_skin`, `user_edu`, `user_name`, `user_sign`, `user_tel`, `dept_id`, `position_id`, `role_id`, `superman`, `holiday`, `pinyin`) VALUES ( NULL, NULL, '1986-02-03 00:00:00', NULL, NULL, '2017-09-22 19:35:40', NULL, 'oasys.jpg', 0, NULL, NULL, NULL, NULL, '123456', 'MH"+numberFormat.format(i)+"', NULL, NULL, '男', 'blue', NULL, 'MH"+numberFormat.format(i)+"', NULL, NULL, 1, 1, 1, 1, NULL, NULL);\n");
		}
	}
	
	
	
	

}
