package cn.gson.oasys.exception;

/**
 * @创建人 luoxiang
 * @创建时间 2019/4/30  18:02
 * @描述
 */

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
public class WebControllerException {

    private static final Logger logger = LoggerFactory.getLogger(WebControllerException.class);

    /**
     * 全局异常捕捉处理
     *
     * @param ex
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Object  errorHandler(Exception ex) {
        logger.error("全局异常处理ERROR--：" + ex.getMessage(),ex);
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);
       // mav.addObject("url", reqest.getRequestURL());
        mav.setViewName("error/400");
        return mav;
    }

    @ResponseBody
    @ExceptionHandler(value = ServiceException.class)
    public Object  errorHandler(ServiceException ex) {
        logger.error("业务异常处理ERROR--：" + ex.getMessage(),ex);
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);
        //mav.addObject("url", reqest.getRequestURL());
        mav.setViewName("error/400");
        return mav;

    }


}
